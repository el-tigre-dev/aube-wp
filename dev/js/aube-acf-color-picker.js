'use strict';

/**
 * Created by abbes on 24/11/2018
 */
jQuery(document).ready(function ($) {
    if (acf.fields.color_picker) {
        // custom colors
        var palette = ['#ffe123', '#4a727f', '#f0c3b1', '#abcce8', '#fbe122', '#000000'];

        // when initially loaded find existing colorpickers and set the palette
        acf.add_action('load', function () {
            $('input.wp-color-picker').each(function () {
                $(this).iris('option', 'palettes', palette);
            });
        });

        // if appended element only modify the new element's palette
        acf.add_action('append', function (el) {
            $(el).find('input.wp-color-picker').iris('option', 'palettes', palette);
        });
    }
});
